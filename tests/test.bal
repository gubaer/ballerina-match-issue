import ballerina/test;

public type BaseError distinct error;

public type SubErrorA distinct BaseError;

public type SubErrorB distinct BaseError;

function generateError(string 'type) returns BaseError {
    match 'type {
        "a" => {
            return error SubErrorA("suberror A");
        }
        "b" => {
            return error SubErrorB("suberror B");
        }
        _ => {
            return error BaseError("base error");
        }

    }
}

@test:Config
public function test01() returns error? {

    BaseError e = generateError("a");

    test:assertTrue(e is SubErrorA);
    test:assertFalse(e is SubErrorB);
}

@test:Config
public function test02() returns error? {

    BaseError e = generateError("a");

    if e is SubErrorA {
        test:assertTrue(true);
    } else if e is SubErrorB {
        test:assertTrue(false);
    }
}

@test:Config
public function test03() returns error? {

    BaseError e = generateError("a");

    match e {
        var errorA if e is SubErrorA => {
            test:assertTrue(true);
        }
        // this creates a 'unreachable pattern' diagnostic in 
        // VS code
        // and a WARNING when built with 'bal build'
        //   [main.bal:(...)] unreachable pattern
        var errorB if e is SubErrorB => {
            test:assertFail("unexpected match clause");
        }
    }
}

@test:Config
public function test04() returns error? {

    BaseError e = generateError("b");

    match e {
        var errorA if e is SubErrorA => {
            test:assertFail("unexpected match clause");
        }
        // this creates a 'unreachable pattern' diagnostic in 
        // VS code
        // and a WARNING when built with 'bal build'
        //   [main.bal:(...)] unreachable pattern
        //
        // But this match clause is cleary reachable, because the
        // unit test passes.
        var errorB if e is SubErrorB => {
            test:assertTrue(true);
        }
    }
}
