Demo repository for an issue reported in the [ballerina-lang](https://github.com/ballerina-platform/ballerina-lang) github repo.

See [issue](https://github.com/ballerina-platform/ballerina-lang/issues/32818).